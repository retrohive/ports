clean:
	find Games -not -name '*.md' -not -name 'PKGBUILD' -not -wholename './.git*' -not -name 'Justfile' -not -name 'description.xml' -not -name 'launcher' -type f -print0 | xargs -0 -n1 rm -v
